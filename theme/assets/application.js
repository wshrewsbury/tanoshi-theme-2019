$(document).ready(function () {

    // NAVBAR TOGGLE FOR MOBILE VIEW
    $(".navbar-burger").click(function () {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });

    // FLICKITY CAROUSEL SETTINGS FOR THE PRODUCT PAGE
    var $productCarousel = $('.product-images.carousel').flickity({
        pageDots: false,
        imagesLoaded: true,
        percentPosition: false
    });

    $productCarousel.on('staticClick.flickity', function (event, pointer, cellElement, cellIndex) {
        // Dismiss if cell wasn't clicked
        if (!cellElement) {
            return;
        }
        // Add .is-clicked to clicked cell, and remove from any currently clicked cell
        $productCarousel.find('.is-clicked').removeClass('is-clicked');
        $(cellElement).addClass('is-clicked');

        // Change the main product image
        $('#ProductPhotoImg').attr('src', $(cellElement).attr('data-fullsize'));
    });
});